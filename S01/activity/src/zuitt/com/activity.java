package zuitt.com;

import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        // Declare variables to store user's information
        String firstName, lastName;
        double firstSubject, secondSubject, thirdSubject;

        // Create a new Scanner object
        Scanner scanner = new Scanner(System.in);

        // Prompt the user for input and gather them using the Scanner methods
        System.out.print("First Name: ");
        firstName = scanner.nextLine();

        System.out.print("Last Name: ");
        lastName = scanner.nextLine();

        System.out.print("First Subject Grade: ");
        firstSubject = scanner.nextDouble();

        System.out.print("Second Subject Grade: ");
        secondSubject = scanner.nextDouble();

        System.out.print("Third Subject Grade: ");
        thirdSubject = scanner.nextDouble();

        // Calculate the average of the user's input
        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        // Print out the user's full name and the average grade
        System.out.println("Good days, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);

        // Close the Scanner object
        scanner.close();
    }
}

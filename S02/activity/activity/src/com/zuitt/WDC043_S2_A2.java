package com.zuitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WDC043_S2_A2 {
    public static void main(String[] args) {
        int[] intArray = {2, 3, 5, 7, 11};
        System.out.println("First prime number is: " + intArray[0]);

        ArrayList<String> friends = new ArrayList<String>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Zoey");
        friends.add("Chloe");

        System.out.println("My friends are: " + friends);

        HashMap<String, Integer> inventories = new HashMap<>();
        inventories.put("toothpaste", 15);
        inventories.put("toothbrush", 20);
        inventories.put("soap", 12);

        System.out.println("Our Current Inventory consists of:");

        for (Map.Entry<String, Integer> entry : inventories.entrySet()) {
            String item = entry.getKey();
            int quantity = entry.getValue();
            System.out.println(item + " = " + quantity);
        }
    }
}

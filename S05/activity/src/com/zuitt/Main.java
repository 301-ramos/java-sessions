package com.zuitt;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact();
        System.out.print("Enter name: ");
        String name1 = scanner.nextLine();
        contact1.setName(name1);
        System.out.print("Enter contact number: ");
        String number1 = scanner.nextLine();
        contact1.setContactNumber(number1);
        System.out.print("Enter address: ");
        String address1 = scanner.nextLine();
        contact1.setAddress(address1);
        phonebook.addContact(contact1);

        System.out.println();
        System.out.print("-----------------------");
        System.out.print("-----------------------");
        System.out.println();

        Contact contact2 = new Contact();
        System.out.print("Enter name: ");
        String name2 = scanner.nextLine();
        contact2.setName(name2);
        System.out.print("Enter contact number: ");
        String number2 = scanner.nextLine();
        contact2.setContactNumber(number2);
        System.out.print("Enter address: ");
        String address2 = scanner.nextLine();
        contact2.setAddress(address2);
        phonebook.addContact(contact2);

        System.out.println();
        System.out.print("-----------------------");
        System.out.print("-----------------------");
        System.out.println();

        System.out.println("Contacts in Phonebook:");
        for (Contact contact : phonebook.getContacts()) {

            if (contact.getContactNumber() != null && !contact.getContactNumber().isEmpty()) {
                System.out.println(contact.getName() + " has the following registered information in our Contact Number:");
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
            } else {
                System.out.println("No contact number available");
            }

            System.out.println();
        }

        scanner.close();
    }
}
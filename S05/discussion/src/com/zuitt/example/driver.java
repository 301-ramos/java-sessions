package com.zuitt.example;

import java.sql.Driver;

public class driver {
    private String name;

    public void setName(String newName) {
        this.name = newName;
    }

    public String getName() {
        return this.name;
    }

    public Driver () {

    }
    public Driver(String name) {
        this.name = name;
    }
}
